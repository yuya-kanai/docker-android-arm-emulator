#!/bin/sh -eu
# Run within container with volume mounted

echo "no" | avdmanager create avd -n test -k "system-images;android-25;google_apis;armeabi-v7a"
emulator -avd test -no-audio -no-boot-anim -accel on -gpu swiftshader_indirect &
